# Social Login with Google and Facebook for David
## Only on general React Native app
#### Your requirement

3 buttons, go to the google, facebook, or linkedin login, when it comes back with the access token for the user on success, get the user details and log them out

## Install and Run

1. clone repository
2. npm install in root folder
3. cd backend and npm install && npm start
4. other terminal, react-native run-ios (run-android) in root folder
